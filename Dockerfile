#Obtener imagen de Node
FROM node:16-alpine3.15
WORKDIR /usr/src/app
COPY package*.json ./

RUN npm install

COPY . .

EXPOSE $PORT

RUN adduser -D richard
USER richard

CMD ["node", "index.js"]
